'use strict';

let request = require('supertest');
let chai = require('chai');
let app = require('../');
let expect = chai.expect;

describe('GET /transformice/profile/Ramonrod', () => {
  it('should return nickname', (done) => {
    request(app)
    .get('/transformice/profile/Ramonrod')
    .end((err, res) => {
      expect(res.body.name).to.equal('Ramonrod');
      expect(res.statusCode).to.equal(200);
      done();
    });
  });
});
