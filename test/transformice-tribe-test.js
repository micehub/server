'use strict';

let request = require('supertest');
let chai = require('chai');
let app = require('../');
let expect = chai.expect;

describe('GET /transformice/tribe/Calma', () => {
  it('should return tribe id', (done) => {
    request(app)
    .get('/transformice/tribe/Calma')
    .end((err, res) => {
      expect(res.body.tribe_id).to.equal('779560');
      expect(res.body).to.not.equal('{"error":"Tribe not found"}');
      done();
    });
  });
});
