'use strict';

// Module dependencies.

let express    = require('express');
let load       = require('express-load');
let bodyParser = require('body-parser');
let app        = express();

// all environments

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));

// load routes, models and controllers

load('models', {cwd: 'src'}).then('controllers').then('routes').into(app);

// starting server

app.listen(process.env.PORT || 8080);
module.exports = app;
