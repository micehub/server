'use strict';

module.exports = (app) => {
    let main = app.controllers.mainController;
    app.get('/', main.index);
};
