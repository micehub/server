'use strict';

module.exports = (app) => {
  let tribe = app.controllers.transformice.tribeController;
  app.get('/transformice/tribe/:tribe', tribe.index);
};
