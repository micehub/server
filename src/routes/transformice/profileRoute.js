'use strict';

module.exports = (app) => {
  let profile = app.controllers.transformice.profileController;
  app.get('/transformice/profile/:username', profile.user);
};
