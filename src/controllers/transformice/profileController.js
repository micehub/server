'use strict';

module.exports = (app) => {
  let profile = {};
  let request = require('request');

  profile.user = (req, res) => {
    let nickname = req.params.username;

    return request(`http://api.micetigri.fr/json/player/${nickname}`, (error, response, body) => {
      if (response.statusCode === 500) {
        return res.status(404).json({message: 'Player not found', statusCode: 404});
      }else {
        return res.status(200).json(JSON.parse(response.body));
      }
    });
  };

  return profile;
};
