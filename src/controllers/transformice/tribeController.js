'use strict';

module.exports = (app) => {
  let tribe = {};
  let request = require('request');

  tribe.index = (req, res) => {
    let tribe = new Buffer(req.params.tribe).toString('base64');

    request(`http://api.formice.com/tribe/stats.json?t=${tribe}`, (error, response, body) =>{
      if (response.body = '{"error":"Tribe not found"}') {
        return res.status(404).json(JSON.parse(body));
      }else {
        return res.status(200).json(JSON.parse(response.body));
      }
    });
  };

  return tribe;
};
