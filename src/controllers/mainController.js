'use strict';

module.exports = (app) => {
  let main = {};

  main.index = (req, res) => {
    res.status(200).json({message: 'Hello World', statusCode: 200});
  };

  return main;
};
